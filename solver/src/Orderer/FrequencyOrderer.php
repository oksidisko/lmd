<?php declare(strict_types=1);

namespace Lamoda\Solver\Orderer;

class FrequencyOrderer
{
    private $itemsFreq = [];
    private $rowFreqRate = [];

    public function order(array $matrix)
    {
        foreach ($matrix as $row) {
            foreach ($row as $itemId => $value) {
                if (!isset($this->itemsFreq[$itemId])) {
                    $this->itemsFreq[$itemId] = 0;
                }

                if ($value === 1) {
                    $this->itemsFreq[$itemId]++;
                }
            }
        }

        foreach ($matrix as $rowId => $row) {
            $this->rowFreqRate[$rowId] = 0;
            foreach ($row as $item => $value) {
                if ($value === 1) {
                    $this->rowFreqRate[$rowId] += ($this->itemsFreq[$item]) / count($row);
                }
            }
        }

        uksort($matrix, [$this, 'compareByFreqRate']);

        return $matrix;
    }

    private function compareByFreqRate($a, $b): int
    {
        if ($this->rowFreqRate[$a] === $this->rowFreqRate[$b]) {
            return 0;
        }

        return ($this->rowFreqRate[$a] < $this->rowFreqRate[$b]) ? -1 : 1;
    }
}
