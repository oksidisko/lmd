<?php declare(strict_types=1);

namespace Lamoda\Solver\Command;

use Lamoda\Solver\Exception\UnsolvableMatrixException;
use Lamoda\Solver\MatrixGenerator;
use Lamoda\Solver\Orderer\FrequencyOrderer;
use Lamoda\Solver\Solver\GreedySolver;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Stopwatch\Stopwatch;

class TestCommand extends Command
{
    private const OPTION_ITERATIONS = 'iterations';
    private const OPTION_CONTAINERS = 'containers';
    private const OPTION_PRODUCTS = 'products';
    private const OPTION_CONTAINER_VOLUME = 'container-volume';

    protected function configure()
    {
        $this->setName('lamoda:solver:test')
            ->addOption(self::OPTION_ITERATIONS, null, InputOption::VALUE_REQUIRED, '', 100)
            ->addOption(self::OPTION_CONTAINERS, null, InputOption::VALUE_REQUIRED, '', 1000)
            ->addOption(self::OPTION_PRODUCTS, null, InputOption::VALUE_REQUIRED, '', 100)
            ->addOption(self::OPTION_CONTAINER_VOLUME, null, InputOption::VALUE_REQUIRED, '', 10)
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->ensureValidInput($input);

        $iterations = (int) $input->getOption(self::OPTION_ITERATIONS);
        $containers = (int) $input->getOption(self::OPTION_CONTAINERS);
        $products = (int) $input->getOption(self::OPTION_PRODUCTS);
        $containerVolume = (int) $input->getOption(self::OPTION_CONTAINER_VOLUME);

        $progressBar = new ProgressBar($output, $iterations);
        $resultCounts = [];
        $unsolvableCounter = 0;

        $matrixGenerator = new MatrixGenerator();
        $matrixOrderer = new FrequencyOrderer();
        $stopwatch = new Stopwatch(true);
        $times = [];
        $orderProgress = 0;
        $solver = new GreedySolver();

        foreach (range(1, $iterations) as $i) {
            $progressBar->advance();

            $matrix = $matrixGenerator->createRandomMatrix($containers, $containerVolume, $products);

            try {
                $stopwatch->start($i);
                $matrix = $matrixOrderer->order($matrix);
                $result = $solver->process($matrix, $products);

                $resultCounts[] = count($result);

                $event = $stopwatch->stop($i);
                $times[] = $event->getDuration();
            } catch (UnsolvableMatrixException $e)  {
                $unsolvableCounter++;
                continue;
            }
        }

        $progressBar->finish();

        $solutionVolumes = array_reduce($resultCounts, function($totals, $value) {
            if (!isset($totals[$value])) {
                $totals[$value] = 0;
            }

            $totals[$value]++;

            return $totals;

        }, []);
        ksort($solutionVolumes);

        $output->writeln('');
        $output->writeln(sprintf('%s iterations run', $iterations));
        if ($unsolvableCounter > 0) {
            $output->writeln(sprintf('Unsolvable in %s cases', $unsolvableCounter));
        }
        $output->writeln(sprintf(
                'Average result volume: %s (min: %s, max: %s)',
                round(array_sum($resultCounts) / count($resultCounts), 2),
                min($resultCounts),
                max($resultCounts)
        ));
        $output->writeln('Result volumes freq:');
        foreach ($solutionVolumes as $count => $volume) {
            $output->writeln(sprintf('%s: %s%%', $count, round(($volume / count($resultCounts)) * 100)));
        }

        $output->writeln(sprintf(
            'Average time (ms): %s (min: %s, max: %s)',
            round(array_sum($times) / ($iterations - $unsolvableCounter), 2),
            min($times),
            max($times)
        ));
    }

    private function ensureValidInput(InputInterface $input)
    {
        $options = [
            self::OPTION_ITERATIONS,
            self::OPTION_CONTAINERS,
            self::OPTION_PRODUCTS,
            self::OPTION_CONTAINER_VOLUME
        ];

        foreach ($options as $option) {
            $value = (int) $input->getOption($option);

            if ($value <= 0) {
                throw new \InvalidArgumentException(sprintf('Option "%s" must be a positive integer', $option));
            }
        }
    }
}
