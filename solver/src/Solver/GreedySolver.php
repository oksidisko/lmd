<?php declare(strict_types=1);

namespace Lamoda\Solver\Solver;

use Lamoda\Solver\Exception\UnsolvableMatrixException;

class GreedySolver
{
    private $selectedRows = [];
    private $selectedItems = [];
    private $matrix = [];
    private $rowSize;

    public function process(array $matrix, int $rowSize)
    {
        $this->matrix = $matrix;
        $this->rowSize = $rowSize;
        $this->selectedRows = [];
        $this->selectedRows = [];
        $this->selectedItems = [];

        while (count($this->selectedItems) < $this->rowSize) {
            $nextRowIndex = $this->findNextRow();

            if ($nextRowIndex === null) {
                throw new UnsolvableMatrixException();
            }

            $this->selectedItems = $this->sum($this->selectedItems, $this->matrix[$nextRowIndex]);
            $this->selectedRows[] = $nextRowIndex;
        }

        return $this->selectedRows;
    }

    private function findNextRow(): ?int
    {
        $maxSum = 0;
        $optimalRow = null;
        foreach ($this->matrix as $k => $row) {
            if (in_array($k, $this->selectedRows)) {
                continue;
            }

            $sum = $this->sum($this->selectedItems, $row);

            if ($sum > $maxSum) {
                $maxSum = $sum;
                $optimalRow = $k;
            }
        }

        return $optimalRow;
    }

    private function sum(array $row1, array $row2): array
    {
        return array_replace($row1, $row2);
    }
}
