<?php declare(strict_types=1);

namespace Lamoda\Solver;

class MatrixGenerator
{
    public function createRandomMatrix(int $subsetsCount, int $subsetVolume, int $itemsCount): array
    {
        $matrix = [];
        for ($i = 0; $i < $subsetsCount; $i++) {
            for ($j = 0; $j < $subsetVolume; $j++) {
                $itemIndex = mt_rand(0, $itemsCount - 1);
                $matrix[$i][$itemIndex] = 1;
            }
        }

        return $matrix;
    }
}
