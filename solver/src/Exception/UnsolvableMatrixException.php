<?php declare(strict_types=1);

namespace Lamoda\Solver\Exception;

class UnsolvableMatrixException extends \InvalidArgumentException
{
}
