<?php declare(strict_types=1);

namespace Tests;

use Lamoda\Solver\Solver\GreedySolver;
use Lamoda\Solver\Orderer\FrequencyOrderer;
use PHPUnit\Framework\TestCase;

class GreedySolverTest extends TestCase
{
    public function testBestSolution()
    {
        $matrix = $this->prepareMatrix([
            [1, 2, 2, 2, 2],
            [1, 2, 3, 4, 5],
            [6, 7, 8, 9, 0],
        ]);

        $solution = (new GreedySolver())->process($matrix, 10);

        $this->assertEquals([
            1, 2
        ], $solution);

    }

    public function testOptimalSolution()
    {
        $matrix = $this->prepareMatrix([
            [1, 2, 2, 2, 2],
            [0, 9, 5, 5, 5],
            [1, 2, 3, 4, 4],
            [6, 7, 8, 9, 0],
        ]);

        $solution = (new GreedySolver())->process($matrix, 10);

        $this->assertEquals([
            3, 2, 1
        ], $solution);
    }

    public function testAntiGreedy()
    {
        $matrix = $this->prepareMatrix([
            [3, 4, 5, 5, 5],
            [1, 2, 3, 3, 3],
            [4, 5, 6, 6, 6],
            [7, 8, 3, 3, 3],
            [9, 9, 8, 8, 8],
            [7, 0, 3, 3, 3],
        ]);

        $solution = (new GreedySolver())->process($matrix, 10);

        $this->assertEquals([
            0, 1, 3, 2, 4, 5
        ], $solution);

        $orderer = new FrequencyOrderer();
        $matrix = $orderer->order($matrix);

        $solution = (new GreedySolver())->process($matrix, 10);

        $this->assertTrue($this->arraysEqual([1, 2, 4, 5], $solution));
    }

    private function prepareMatrix(array $source): array
    {
        $matrix = [];

        foreach ($source as $k => $row) {
            $matrix[$k] = [];
            foreach ($row as $item) {
                $matrix[$k][$item] = 1;
            }
        }

        return $matrix;
    }

    private function arraysEqual(array $a, array $b)
    {
        if (count($a) !== count($b)) {
            return false;
        }

        if (count(array_diff($a, $b))) {
            return false;
        }

        return true;
    }
}
