<?php declare(strict_types=1);

namespace Lamoda\Store\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Lamoda\Store\Entity\Product;

class ProductRepository
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var EntityRepository */
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Product::class);
    }

    public function find(string $id): ?Product
    {
        return $this->repository->find($id);
    }

    public function save(Product $container): void
    {
        $this->em->persist($container);
        $this->em->flush();
    }
}
