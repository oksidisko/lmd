<?php declare(strict_types=1);

namespace Lamoda\Store\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Lamoda\Store\Entity\Container;

class ContainerRepository
{
    /** @var EntityManagerInterface */
    private $em;

    /** @var EntityRepository */
    private $repository;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->repository = $this->em->getRepository(Container::class);
    }

    public function find(string $id): ?Container
    {
        return $this->repository->find($id);
    }

    /**
     * @return Container[]
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }

    public function save(Container $container): void
    {
        $this->em->persist($container);
        $this->em->flush();
    }
}
