<?php declare(strict_types=1);

namespace Lamoda\Store\Command\Denormalizer;

use Lamoda\Store\Command\Dto\ContainerDto;
use Lamoda\Store\Command\Dto\ProductDto;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerAwareTrait;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class ContainerDenormalizer implements DenormalizerInterface, DenormalizerAwareInterface
{
    use DenormalizerAwareTrait;

    public function denormalize($data, $class, $format = null, array $context = [])
    {
        $products = [];
        if (isset($data['products']) && is_array($data['products'])) {
            foreach ($data['products'] as $productData) {
                $products[] = new ProductDto($productData['id'], $productData['name']);
            }
        }

        return new ContainerDto(
            $data['id'],
            $data['name'],
            $products
        );
    }

    public function supportsDenormalization($data, $type, $format = null)
    {
        return $type === ContainerDto::class;
    }
}
