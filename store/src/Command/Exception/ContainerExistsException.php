<?php declare(strict_types=1);

namespace Lamoda\Store\Command\Exception;

use Throwable;

class ContainerExistsException extends \RuntimeException
{
    public function __construct(string $id, int $code = 0, Throwable $previous = null)
    {
        parent::__construct(sprintf('Container "%s" already exists', $id), $code, $previous);
    }
}
