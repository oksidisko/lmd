<?php declare(strict_types=1);

namespace Lamoda\Store\Command;

use Lamoda\Store\Command\Dto\ContainerDto;
use Lamoda\Store\Command\Exception\ContainerExistsException;
use Lamoda\Store\Entity\Container;
use Lamoda\Store\Entity\Product;
use Lamoda\Store\Repository\ContainerRepository;
use Lamoda\Store\Repository\ProductRepository;

class ContainerAddCommand
{
    /** @var ContainerRepository */
    private $containerRepository;

    /** @var ProductRepository */
    private $productRepository;

    public function __construct(ContainerRepository $containerRepository, ProductRepository $productRepository)
    {
        $this->containerRepository = $containerRepository;
        $this->productRepository = $productRepository;
    }

    public function execute(ContainerDto $containerDto): void
    {
        $this->ensureContainerNotExists($containerDto);

        $container = new Container(
            $containerDto->getId(), $containerDto->getName()
        );

        foreach ($containerDto->getProducts() as $productDto) {
            if (null === $product = $this->productRepository->find($productDto->getId())) {
                $product = new Product($productDto->getId(), $productDto->getName());
            }

            $container->addProduct($product);
        }

        $this->containerRepository->save($container);
    }

    private function ensureContainerNotExists(ContainerDto $containerDto): void
    {
        $container = $this->containerRepository->find($containerDto->getId());

        if ($container !== null) {
            throw new ContainerExistsException($container->getId());
        }
    }
}
