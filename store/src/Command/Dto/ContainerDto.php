<?php declare(strict_types=1);

namespace Lamoda\Store\Command\Dto;

class ContainerDto
{
    private $id;
    private $name;
    private $products;

    public function __construct(string $id, string $name, array $products)
    {
        $this->id = $id;
        $this->name = $name;
        $this->products = $products;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return ProductDto[]
     */
    public function getProducts(): array
    {
        return $this->products;
    }
}
