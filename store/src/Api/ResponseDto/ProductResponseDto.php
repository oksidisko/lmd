<?php declare(strict_types=1);

namespace Lamoda\Store\Api\ResponseDto;

class ProductResponseDto
{
    private $id;
    private $name;

    public function __construct(string $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }
}
