<?php declare(strict_types=1);

namespace Lamoda\Store\Api\ResponseDto;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;

class CommonResponseDto
{
    private $code;

    private $message;

    public function __construct(int $code, string $message)
    {
        $this->code = $code;
        $this->message = $message;
    }

    public static function fromException(\Exception $exception)
    {
        if ($exception instanceof HttpExceptionInterface) {
            $code = $exception->getStatusCode();
        } else {
            $code = $exception->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR;
        }

        return new static(
            $code,
            $exception->getMessage()
        );
    }

    public function getCode(): int
    {
        return $this->code;
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
