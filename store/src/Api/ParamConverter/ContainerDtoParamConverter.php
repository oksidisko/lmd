<?php declare(strict_types=1);

namespace Lamoda\Store\Api\ParamConverter;

use Lamoda\Store\Command\Dto\ContainerDto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

class ContainerDtoParamConverter implements ParamConverterInterface
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        $request->attributes->set(
            $configuration->getName(),
            $this->serializer->deserialize($request->getContent(), ContainerDto::class, 'json')
        );
    }

    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === ContainerDto::class;
    }
}
