<?php declare(strict_types=1);

namespace Lamoda\Store\Api\ParamConverter;

use Lamoda\Store\Entity\Container;
use Lamoda\Store\Repository\ContainerRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Request\ParamConverter\ParamConverterInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ContainerParamConverter implements ParamConverterInterface
{
    /** @var ContainerRepository */
    private $containerRepository;

    public function __construct(ContainerRepository $containerRepository)
    {
        $this->containerRepository = $containerRepository;
    }

    public function apply(Request $request, ParamConverter $configuration)
    {
        if (null === $container = $this->containerRepository->find($request->get('id'))) {
            throw new NotFoundHttpException('Container not found');
        }

        $request->attributes->set($configuration->getName(), $container);
    }

    public function supports(ParamConverter $configuration)
    {
        return $configuration->getClass() === Container::class;
    }
}
