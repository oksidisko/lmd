<?php declare(strict_types=1);

namespace Lamoda\Store\Api\Event\Subscriber;

use Lamoda\Store\Api\ResponseDto\CommonResponseDto;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\SerializerInterface;

class ResponseExceptionSubscriber implements EventSubscriberInterface
{
    /** @var SerializerInterface */
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => ['onKernelException', 30]
        ];
    }

    public function onKernelException(ExceptionEvent $event): void
    {
        $responseDto = CommonResponseDto::fromException($event->getException());

        $response = (new JsonResponse())
            ->setStatusCode($responseDto->getCode())
            ->setContent($this->serializer->serialize($responseDto, 'json'));

        $event->setResponse($response);
    }
}
