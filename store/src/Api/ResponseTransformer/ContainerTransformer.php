<?php declare(strict_types=1);

namespace Lamoda\Store\Api\ResponseTransformer;

use Lamoda\Store\Api\ResponseDto\ContainerResponseDto;
use Lamoda\Store\Api\ResponseDto\ProductResponseDto;
use Lamoda\Store\Entity\Container;

class ContainerTransformer
{
    public function entityToDto(Container $container): ContainerResponseDto
    {
        $products = [];
        foreach ($container->getProducts() as $product) {
            $products[] = new ProductResponseDto($product->getId(), $product->getName());
        }

        return new ContainerResponseDto(
            $container->getId(),
            $container->getName(),
            $products
        );
    }
}
