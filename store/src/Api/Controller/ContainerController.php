<?php declare(strict_types=1);

namespace Lamoda\Store\Api\Controller;

use Lamoda\Store\Api\ResponseDto\ContainerResponseDto;
use Lamoda\Store\Api\ResponseTransformer\ContainerTransformer;
use Lamoda\Store\Command\ContainerAddCommand;
use Lamoda\Store\Command\Dto\ContainerDto;
use Lamoda\Store\Entity\Container;
use Lamoda\Store\Repository\ContainerRepository;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Routing\Annotation\Route;
use Swagger\Annotations as SWG;

/**
 * @Route("/containers")
 */
class ContainerController
{
    /** @var ContainerAddCommand */
    private $containerAddCommand;

    /** @var ContainerRepository */
    private $containerRepository;

    /** @var ContainerTransformer */
    private $containerTransformer;

    public function __construct(
        ContainerAddCommand $containerAddCommand,
        ContainerRepository $containerRepository,
        ContainerTransformer $containerTransformer

    ) {
        $this->containerAddCommand = $containerAddCommand;
        $this->containerRepository = $containerRepository;
        $this->containerTransformer = $containerTransformer;
    }

    /**
     * @Route("", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns containers list",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Container::class))
     *     )
     * )
     */
    public function listAction()
    {
        return array_map(function($container) {
            return $this->containerTransformer->entityToDto($container);
        }, $this->containerRepository->findAll());
    }

    /**
     * @Route("/{id}", methods={"GET"})
     * @SWG\Response(
     *     response=200,
     *     description="Returns one container by id",
     *      @Model(type=Container::class)
     * )
     *
     * @param Container $container
     * @return ContainerResponseDto
     */
    public function oneAction(Container $container)
    {
        return $this->containerTransformer->entityToDto($container);
    }

    /**
     * @Route("", methods={"PUT"})
     * @param ContainerDto $containerDto
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     description="Container model",
     *     required=true,
     *     @Model(type=ContainerDto::class),
     *   )
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns one container by id",
     *      @Model(type=Container::class)
     * )
     *
     *
     * @return ContainerResponseDto
     */
    public function addAction(ContainerDto $containerDto): ContainerResponseDto
    {
        $this->containerAddCommand->execute($containerDto);

        $container = $this->containerRepository->find($containerDto->getId());

        return $this->containerTransformer->entityToDto($container);
    }
}
